$(function(){
	function loadData(path){
		$.getJSON(path+"/storage.json",function(data){
			console.log(data);
			loadTable(prepareData(data));
		})
	}

	
	loadData('.');
});
function loadTable(data){
	$.each(data, function(i,r){
		var row = $("<tr/>");
		$.each(r, function(j,v){
			row.append($("<td/>").text(v==null?"нет данных":v));
		});	
		$('table').append(row);
	})
}

function prepareData(data){
	var res=[];
	$.each(data.users,function(i,v){
		var person = getItemById(data.persons, "person", v.person_id);		
		var position = getItemById(data.positions, "position", v.position_id);		
		var department = getItemById(data.departments, "department", v.department_id);			
		var company = getItemById(data.companys, "company", department.company_id);		
		var item = $.extend({},v,person,position,department,company);		
		var row =[
			item.last_name+" "+item.first_name+" "+item.middle_name,
			item.company_name,
			item.department_name,
			item.position_name,
			item.phone_number,
			item.email
		]
		res.push(row);
	});		
	return res;
}

function getItemById(objs, name, id){	
	return $(objs).filter( function(i,v){			
		return v[name+"_id"]==id;
	})[0];
}